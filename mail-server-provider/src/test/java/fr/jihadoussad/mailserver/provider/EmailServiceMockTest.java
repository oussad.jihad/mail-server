package fr.jihadoussad.mailserver.provider;

import fr.jihadoussad.mailserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.mailserver.contract.EmailService;
import fr.jihadoussad.mailserver.contract.input.MailSenderInput;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static fr.jihadoussad.tools.api.response.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
public class EmailServiceMockTest {

    @TestConfiguration
    static class EmailServiceSimuTestConfiguration {

        @Bean
        public EmailService emailService() {
            return new EmailServiceMock();
        }
    }

    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private EmailService emailService;

    @Test
    public void sendMailWhenFromNullTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo@test.com");

        final ContractValidationException e = assertThrows(ContractValidationException.class, () -> emailService.sendEmail(input));
        assertThat(e.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void sendMailWhenToNullTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setTo("foo@test.com");

        final ContractValidationException e = assertThrows(ContractValidationException.class, () -> emailService.sendEmail(input));
        assertThat(e.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void sendMailWhenFromAddressIncorrectTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo");
        input.setTo("foo@test.com");

        final ContractValidationException e = assertThrows(ContractValidationException.class, () -> emailService.sendEmail(input));
        assertThat(e.getCode()).isEqualTo(INVALID_MAIL.code);
    }

    @Test
    public void sendMailWhenToAddressIncorrectTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo@test.com");
        input.setTo("foo");

        final ContractValidationException e = assertThrows(ContractValidationException.class, () -> emailService.sendEmail(input));
        assertThat(e.getCode()).isEqualTo(INVALID_MAIL.code);
    }

    @Test
    public void sendMailWhenSubjectNullTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo@test.com");
        input.setTo("foo@test.com");

        final ContractValidationException e = assertThrows(ContractValidationException.class, () -> emailService.sendEmail(input));
        assertThat(e.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void sendMailWhenTextNullTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo@test.com");
        input.setTo("foo@test.com");
        input.setSubject("foo");

        final ContractValidationException e = assertThrows(ContractValidationException.class, () -> emailService.sendEmail(input));
        assertThat(e.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void sendMailWhenRuntimeExceptionTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo@test.com");
        input.setTo("HTTP_500@test.com");
        input.setSubject("foo");
        input.setText("foo");

        assertThrows(RuntimeException.class, () -> emailService.sendEmail(input));
    }


    @Test
    public void sendMailTest() throws ContractValidationException {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo@test.com");
        input.setTo("foo@test.com");
        input.setSubject("foo");
        input.setText("foo");

        emailService.sendEmail(input);
    }
}
