package fr.jihadoussad.mailserver.provider;

import fr.jihadoussad.mailserver.contract.input.MailSenderInput;
import fr.jihadoussad.mailserver.contract.exceptions.ContractValidationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static fr.jihadoussad.tools.api.response.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;

@ExtendWith(MockitoExtension.class)
public class EmailServiceImplTest {

    @Mock
    private JavaMailSender mailSender;

    @InjectMocks
    private EmailServiceImpl emailService;

    @Test
    public void sendMailWhenFromNullTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo@test.com");

        final ContractValidationException e = assertThrows(ContractValidationException.class, () -> emailService.sendEmail(input));
        assertThat(e.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void sendMailWhenToNullTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setTo("foo@test.com");

        final ContractValidationException e = assertThrows(ContractValidationException.class, () -> emailService.sendEmail(input));
        assertThat(e.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void sendMailWhenFromAddressIncorrectTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo");
        input.setTo("foo@test.com");

        final ContractValidationException e = assertThrows(ContractValidationException.class, () -> emailService.sendEmail(input));
        assertThat(e.getCode()).isEqualTo(INVALID_MAIL.code);
    }

    @Test
    public void sendMailWhenToAddressIncorrectTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo@test.com");
        input.setTo("foo");

        final ContractValidationException e = assertThrows(ContractValidationException.class, () -> emailService.sendEmail(input));
        assertThat(e.getCode()).isEqualTo(INVALID_MAIL.code);
    }

    @Test
    public void sendMailWhenSubjectNullTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo@test.com");
        input.setTo("foo@test.com");

        final ContractValidationException e = assertThrows(ContractValidationException.class, () -> emailService.sendEmail(input));
        assertThat(e.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void sendMailWhenTextNullTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo@test.com");
        input.setTo("foo@test.com");
        input.setSubject("foo");

        final ContractValidationException e = assertThrows(ContractValidationException.class, () -> emailService.sendEmail(input));
        assertThat(e.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void sendMailWhenSendMailExceptionTest() {
        doThrow(MailSendException.class).when(mailSender).send(any(SimpleMailMessage.class));

        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo@test.com");
        input.setTo("foo@test.com");
        input.setSubject("foo");
        input.setText("foo");

        final ContractValidationException e = assertThrows(ContractValidationException.class, () -> emailService.sendEmail(input));
        assertThat(e.getCode()).isEqualTo(INVALID_MAIL.code);
    }

    @Test
    public void sendMailTest() throws ContractValidationException {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo@test.com");
        input.setTo("foo@test.com");
        input.setSubject("foo");
        input.setText("foo");

        emailService.sendEmail(input);
    }
}
