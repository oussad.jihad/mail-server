package fr.jihadoussad.mailserver.provider;

import fr.jihadoussad.mailserver.contract.*;
import fr.jihadoussad.mailserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.mailserver.contract.input.MailSenderInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import static fr.jihadoussad.tools.api.response.ResponseCode.INVALID_MAIL;

@Service("email-service-prod")
public class EmailServiceImpl implements EmailService {

    private final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);

    private final JavaMailSender mailSender;

    public EmailServiceImpl(final JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void sendEmail(final MailSenderInput input) throws ContractValidationException {
        try {
            // Validate input
            logger.debug("Validate mail sender input: {}", input);
            input.validate();

            // Prepare
            logger.debug("Build mail message to send...");
            final SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(input.getFrom());
            message.setTo(input.getTo());
            message.setSubject(input.getSubject());
            message.setText(input.getText());

            // Process
            logger.debug("Call mailSender to send following message: {}", message);
            mailSender.send(message);
        } catch (final MailSendException e) {
            logger.debug(e.toString());
            throw new ContractValidationException(INVALID_MAIL.message, INVALID_MAIL.code);
        }
    }
}
