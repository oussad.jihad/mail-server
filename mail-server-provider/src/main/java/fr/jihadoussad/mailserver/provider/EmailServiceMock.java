package fr.jihadoussad.mailserver.provider;

import fr.jihadoussad.mailserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.mailserver.contract.EmailService;
import fr.jihadoussad.mailserver.contract.input.MailSenderInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Service("email-service-simu")
public class EmailServiceMock implements EmailService {

    private final Logger logger = LoggerFactory.getLogger(EmailServiceMock.class);

    private static final String HTTP_500 = "HTTP_500@test.com";

    @Override
    public void sendEmail(MailSenderInput input) throws ContractValidationException {
        // Validate input
        logger.debug("Validate mail sender input: {}", input);
        input.validate();

        if (HTTP_500.equals(input.getTo())) {
            logger.debug("Discriminant mail server detected to mock problem server: {}", input.getTo());
            throw new RuntimeException();
        }
    }
}
