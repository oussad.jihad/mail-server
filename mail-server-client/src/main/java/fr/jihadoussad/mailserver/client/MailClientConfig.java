package fr.jihadoussad.mailserver.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import fr.jihadoussad.mailserver.client.context.MailClientContext;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MailClientConfig {

	private final Logger logger = LoggerFactory.getLogger(MailClientConfig.class);

	@Bean
	MailClient mailClient(final ApplicationContext context, final MailClientContext mailClientContext) {
		logger.debug("Set application context with following mail client provider: {}", mailClientContext.getProvider());
		return (MailClient) context.getBean(mailClientContext.getProvider());
	}
}
