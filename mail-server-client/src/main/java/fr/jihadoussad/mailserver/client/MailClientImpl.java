package fr.jihadoussad.mailserver.client;

import fr.jihadoussad.mailserver.client.context.MailClientContext;
import fr.jihadoussad.mailserver.contract.input.MailSenderInput;
import fr.jihadoussad.tools.api.rest.RestClient;
import fr.jihadoussad.tools.api.rest.RestClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component("mail-client-prod")
class MailClientImpl implements MailClient {

    private final Logger logger = LoggerFactory.getLogger(MailClientImpl.class);

    private final String mailServerUrl;

    private final RestClient restClient;

    MailClientImpl(final MailClientContext mailClientContext) {
        this.mailServerUrl = mailClientContext.getUrl();
        this.restClient = RestClientBuilder.getInstance()
            .withRestTemplate(restTemplateBuilder().build())
            .build();
    }

    private RestTemplateBuilder restTemplateBuilder() {
        final RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        final MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Collections.singletonList(MediaType.APPLICATION_JSON));

        restTemplateBuilder.additionalMessageConverters(mappingJackson2HttpMessageConverter);

        return restTemplateBuilder;
    }

    @Override
    public ResponseEntity<Void> sendEmail(final MailSenderInput input) {
        final String urlRequest = mailServerUrl + "/send";
        logger.debug("Calling {} with following request body: {}", urlRequest, input);
        return restClient.getResponseEntity(urlRequest, HttpMethod.POST, input, new ParameterizedTypeReference<Void>() {});
    }
}
