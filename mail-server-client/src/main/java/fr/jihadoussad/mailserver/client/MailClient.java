package fr.jihadoussad.mailserver.client;

import fr.jihadoussad.mailserver.contract.input.MailSenderInput;
import org.springframework.http.ResponseEntity;

/**
 * Mail Client
 */
public interface MailClient {

	/**
	 * Call client server to send an email
	 * @param input the mail input
	 */
	ResponseEntity<Void> sendEmail(final MailSenderInput input);
}
