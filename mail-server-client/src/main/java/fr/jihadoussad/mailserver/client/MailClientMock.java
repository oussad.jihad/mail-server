package fr.jihadoussad.mailserver.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.mailserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tools.api.response.ErrorOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.stereotype.Component;

import fr.jihadoussad.mailserver.contract.input.MailSenderInput;
import org.springframework.web.client.HttpClientErrorException;

import java.nio.charset.StandardCharsets;

import static fr.jihadoussad.tools.api.response.ResponseCode.TECHNICAL_ERROR;

@Component("mail-client-mock")
@PropertySource(value = "classpath:mail-mock.properties", ignoreResourceNotFound = true)
class MailClientMock implements MailClient {

	private final Logger logger = LoggerFactory.getLogger(MailClientMock.class);

	private final ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json().build();

	@Value("${mail-error:null}")
	private String mailError;

	@Override
	public ResponseEntity<Void> sendEmail(final MailSenderInput input) {
		try {
			// Validate
			logger.debug("Validate mail sender input: {}", input);
			input.validate();

			// Check mail error
			if (input.getTo().equals(mailError)) {
				logger.debug("Discriminant mail server detected to mock problem server: {}", input.getTo());
				throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "", handleError(TECHNICAL_ERROR.message, TECHNICAL_ERROR.code), null);
			}

			// Return
			return ResponseEntity.ok().build();
		} catch (final ContractValidationException exception) {
			logger.debug(exception.toString());
			throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "", handleError(exception.getMessage(), exception.getCode()), null);
		}
	}

	private byte[] handleError(final String message, final String code) {
		try {
			final ErrorOutput errorOutput = new ErrorOutput();
			errorOutput.setCode(code);
			errorOutput.setMessage(message);
			return objectMapper.writeValueAsString(errorOutput).getBytes(StandardCharsets.UTF_8);
		} catch (final JsonProcessingException e) {
			return new byte[0];
		}
	}
}
