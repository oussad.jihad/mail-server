package fr.jihadoussad.mailserver.client.context;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "mail-client")
public class MailClientContext {

	private String provider;

	private String url;

	public MailClientContext() {
		this.provider = "mail-client-prod";
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(final String provider) {
		this.provider = provider;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}
}
