package fr.jihadoussad.mailserver.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.mailserver.contract.input.MailSenderInput;
import fr.jihadoussad.tools.api.response.ErrorOutput;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.client.HttpClientErrorException;

import static fr.jihadoussad.tools.api.response.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, properties = {"mail-client.provider=mail-client-mock"})
public class MailClientMockIntegrationTest {

    @Autowired
    private MailClient mailClient;

    private static final ObjectMapper OBJECT_MAPPER = Jackson2ObjectMapperBuilder.json().build();

    @Test
    public void sendMailWhenMissingMandatoryFieldInputTest() throws JsonProcessingException {
        final MailSenderInput input = new MailSenderInput();

        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> mailClient.sendEmail(input));
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void sendMailWhenMailInvalidTest() throws JsonProcessingException {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo");
        input.setTo("foo@test.com");

        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> mailClient.sendEmail(input));
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(INVALID_MAIL.code);
    }

    @Test
    public void sendMailWhenTechnicalErrorTest() throws JsonProcessingException {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo@test.com");
        input.setTo("HTTP_500@test.com");
        input.setSubject("foo");
        input.setText("foo");

        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> mailClient.sendEmail(input));
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(TECHNICAL_ERROR.code);
    }


    @Test
    public void sendMailTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("foo@test.com");
        input.setTo("foo@test.com");
        input.setSubject("foo");
        input.setText("foo");

        final ResponseEntity<Void> response = mailClient.sendEmail(input);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}
