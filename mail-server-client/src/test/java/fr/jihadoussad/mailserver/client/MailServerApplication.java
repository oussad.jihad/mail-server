package fr.jihadoussad.mailserver.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@SpringBootApplication
@ComponentScan(basePackages = "fr.jihadoussad.mailserver.*")
public class MailServerApplication {

    public static void main(final String[] args) {
        SpringApplication.run(MailServerApplication.class);
    }

    @Bean
    JavaMailSender mailSender() {
        return new JavaMailSenderImpl();
    }
}
