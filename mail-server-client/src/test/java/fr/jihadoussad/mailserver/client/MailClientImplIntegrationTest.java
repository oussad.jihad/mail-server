package fr.jihadoussad.mailserver.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.mailserver.contract.input.MailSenderInput;
import fr.jihadoussad.tools.api.response.ErrorOutput;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import static fr.jihadoussad.tools.api.response.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class MailClientImplIntegrationTest {

    @Autowired
    private MailClient mailClient;

    private static final ObjectMapper OBJECT_MAPPER = Jackson2ObjectMapperBuilder.json().build();

    @Test
    public void sendEmailWhenMissingMandatoryFieldInputTest() throws JsonProcessingException {
        final MailSenderInput input = new MailSenderInput();
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> mailClient.sendEmail(input));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
    }

    @Test
    public void sendEmailWhenMailInvalidTest() throws JsonProcessingException {
        final MailSenderInput input = createMailSenderInput();
        input.setTo("fake-email");
        final HttpClientErrorException response = assertThrows(HttpClientErrorException.class, () -> mailClient.sendEmail(input));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getMessage()).isEqualTo(INVALID_MAIL.message);
        assertThat(output.getCode()).isEqualTo(INVALID_MAIL.code);
    }

    @Test
    public void sendEmailWhenTechnicalErrorTest() throws JsonProcessingException {
        final MailSenderInput input = createMailSenderInput();
        input.setTo("HTTP_500@test.com");
        final HttpServerErrorException response = assertThrows(HttpServerErrorException.class, () -> mailClient.sendEmail(input));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(response.getResponseBodyAsString()).isNotNull();

        final ErrorOutput output = OBJECT_MAPPER.readValue(response.getResponseBodyAsString(), ErrorOutput.class);
        assertThat(output.getMessage()).isEqualTo(TECHNICAL_ERROR.message);
        assertThat(output.getCode()).isEqualTo(TECHNICAL_ERROR.code);
    }

    @Test
    public void sendEmailTest() {
        final ResponseEntity<Void> response = mailClient.sendEmail(createMailSenderInput());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private MailSenderInput createMailSenderInput() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("from@test.com");
        input.setTo("to@test.com");
        input.setSubject("[Subject]");
        input.setText("test");

        return input;
    }
}
