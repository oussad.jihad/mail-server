package fr.jihadoussad.mailserver.controller;
import fr.jihadoussad.mailserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.mailserver.contract.EmailService;
import fr.jihadoussad.mailserver.contract.input.MailSenderInput;
import fr.jihadoussad.mailserver.controller.context.MailServerContext;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MailController {

    private final Logger logger = LoggerFactory.getLogger(MailController.class);

    private final EmailService emailService;

    public MailController(final ApplicationContext context, final MailServerContext mailServerContext) {
        logger.debug("Set application context with following mail server provider: {}", mailServerContext.getProvider());
        this.emailService = (EmailService) context.getBean(mailServerContext.getProvider());
    }

    @Operation(description = "Send an email", operationId = "sendEmail", responses = {
            @ApiResponse(responseCode = "200", description="Successful app register"),
            @ApiResponse(responseCode = "400", description="Contract input violation"),
            @ApiResponse(responseCode = "500", description = "Technical error")
    })
    @PostMapping(value = "/send", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> sendEmail(@RequestBody final MailSenderInput input) throws ContractValidationException {
        logger.info("BEGIN - Try to send an email");

        emailService.sendEmail(input);

        logger.info("END - Email successfully sent.");

        return ResponseEntity.ok().build();
    }


}
