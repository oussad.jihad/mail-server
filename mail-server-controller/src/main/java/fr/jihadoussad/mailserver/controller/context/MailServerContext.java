package fr.jihadoussad.mailserver.controller.context;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "mail-server")
public class MailServerContext {

    private String provider;

    public MailServerContext() {
        this.provider = "email-service-prod";
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(final String provider) {
        this.provider = provider;
    }
}
