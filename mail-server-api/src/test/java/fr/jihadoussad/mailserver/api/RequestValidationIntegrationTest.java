package fr.jihadoussad.mailserver.api;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RequestValidationIntegrationTest extends CommonIntegrationTest {

    @Test
    public void generate_httpRequestMethodNotSupportedExceptionTest() throws Exception {
        mvc.perform(get(SEND_URL)
                .header("Access-Control-Request-Method", "GET"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void generate_httpMediaTypeNotSupportedTest() throws Exception {
        mvc.perform(post(SEND_URL)
                .header("Access-Control-Request-Method", "POST"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void generate_missingRequestBodyTest() throws Exception {
        mvc.perform(post(SEND_URL)
                .header("Access-Control-Request-Method", "POST")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

}
