package fr.jihadoussad.mailserver.api;

import fr.jihadoussad.mailserver.contract.input.MailSenderInput;
import fr.jihadoussad.tools.api.response.ErrorOutput;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static fr.jihadoussad.tools.api.response.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
public class MailServerApplicationIntegrationTest extends CommonIntegrationTest {

    @TestConfiguration
    static class MailServerTestConfiguration {
        @Bean
        JavaMailSender javaMailSender() {
            return new JavaMailSenderImpl();
        }
    }

    @Test
    public void sendEmailWhenFromInputNullTest() throws Exception {
        final MailSenderInput input = new MailSenderInput();
        final ErrorOutput result = objectMapper.readValue(jsonResponse(input, status().isBadRequest()), ErrorOutput.class);

        assertThat(result.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(result.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "from null");
    }

    @Test
    public void sendEmailWhenToInputNullTest() throws Exception {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("from");
        final ErrorOutput result = objectMapper.readValue(jsonResponse(input, status().isBadRequest()), ErrorOutput.class);

        assertThat(result.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(result.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "to null");
    }

    @Test
    public void sendEmailWhenInvalidMailInputTest() throws Exception {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("from");
        input.setTo("to");
        final ErrorOutput result = objectMapper.readValue(jsonResponse(input, status().isBadRequest()), ErrorOutput.class);

        assertThat(result.getCode()).isEqualTo(INVALID_MAIL.code);
        assertThat(result.getMessage()).isEqualTo(INVALID_MAIL.message);
    }

    @Test
    public void sendEmailWhenSubjectInputNullTest() throws Exception {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("from@test.com");
        input.setTo("to@test.com");
        final ErrorOutput result = objectMapper.readValue(jsonResponse(input, status().isBadRequest()), ErrorOutput.class);

        assertThat(result.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(result.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "subject null");
    }

    @Test
    public void sendEmailWhenTextInputNullTest() throws Exception {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("from@test.com");
        input.setTo("to@test.com");
        input.setSubject("[Subject]");
        final ErrorOutput result = objectMapper.readValue(jsonResponse(input, status().isBadRequest()), ErrorOutput.class);

        assertThat(result.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(result.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "text null");
    }

    @Test
    public void sendEmailWhenInternalErrorTest() throws Exception {
        final MailSenderInput input = createMailSenderInput();
        input.setTo("HTTP_500@test.com");

        jsonResponse(input, status().isInternalServerError());
    }

    @Test
    public void sendEmailTest() throws Exception {
        jsonResponse(createMailSenderInput(), status().isOk());
    }

    private MailSenderInput createMailSenderInput() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("from@test.com");
        input.setTo("to@test.com");
        input.setSubject("[Subject]");
        input.setText("test");

        return input;
    }
}
