package fr.jihadoussad.mailserver.api;

import fr.jihadoussad.tools.api.logger.Slf4jMDCFilterConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"fr.jihadoussad.mailserver.*"}, basePackageClasses = {Slf4jMDCFilterConfiguration.class})
public class MailServerApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(final SpringApplicationBuilder builder) {
        return builder.sources(MailServerApplication.class);
    }

    public static void main(final String[] args) {
        SpringApplication.run(MailServerApplication.class, args);
    }
}
