package fr.jihadoussad.mailserver.contract.input;

import fr.jihadoussad.mailserver.contract.Validable;
import fr.jihadoussad.mailserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tools.validators.CommonRegexValidator;
import fr.jihadoussad.tools.validators.InputValidator;

import java.util.StringJoiner;

import static fr.jihadoussad.tools.api.response.ResponseCode.INVALID_MAIL;
import static fr.jihadoussad.tools.api.response.ResponseCode.MISSING_MANDATORY_FIELD;

public class MailSenderInput implements Validable {

    private String from;

    private String to;

    private String subject;

    private String text;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public void validate() throws ContractValidationException {
        if (from == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "from null", MISSING_MANDATORY_FIELD.code);
        }
        if (to == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "to null", MISSING_MANDATORY_FIELD.code);
        }
        final InputValidator mailValidator = InputValidator.getInstance(CommonRegexValidator.MAIL.pattern);
        if (!(mailValidator.validate(from) && mailValidator.validate(to))) {
            throw new ContractValidationException(INVALID_MAIL.message, INVALID_MAIL.code);
        }
        if (subject == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "subject null", MISSING_MANDATORY_FIELD.code);
        }
        if (text == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "text null", MISSING_MANDATORY_FIELD.code);
        }
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", MailSenderInput.class.getSimpleName() + "[", "]")
                .add("from='" + from + "'")
                .add("to='" + to + "'")
                .add("subject='" + subject + "'")
                .add("text='" + text + "'")
                .toString();
    }
}
