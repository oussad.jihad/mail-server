package fr.jihadoussad.mailserver.contract;

import fr.jihadoussad.mailserver.contract.exceptions.ContractValidationException;

/**
 * Validable
 */
public interface Validable {

    void validate() throws ContractValidationException;
}
