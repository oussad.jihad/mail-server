package fr.jihadoussad.mailserver.contract;

import fr.jihadoussad.mailserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.mailserver.contract.input.MailSenderInput;

/**
 * Email Service
 */
public interface EmailService {

    /**
     * Send an email
     * @param input from + to + subject + content text
     * @throws ContractValidationException if from or to mail is invalid
     */
    void sendEmail(final MailSenderInput input) throws ContractValidationException;
}
