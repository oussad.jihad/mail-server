package fr.jihadoussad.mailserver.contract.input;

import fr.jihadoussad.mailserver.contract.exceptions.ContractValidationException;
import org.junit.jupiter.api.Test;

import static fr.jihadoussad.tools.api.response.ResponseCode.INVALID_MAIL;
import static fr.jihadoussad.tools.api.response.ResponseCode.MISSING_MANDATORY_FIELD;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MailSenderInputTest {

    @Test
    public void validateWithMissingMandatoryFromTest() {
        final MailSenderInput input = new MailSenderInput();
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "from null");
    }

    @Test
    public void validateWithMissingMandatoryToTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("from");
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "to null");
    }

    @Test
    public void validateWithInvalidMailFromTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("from");
        input.setTo("to");
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(INVALID_MAIL.code);
        assertThat(exception.getMessage()).isEqualTo(INVALID_MAIL.message);
    }

    @Test
    public void validateWithInvalidMailToTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("from@test.com");
        input.setTo("to");
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(INVALID_MAIL.code);
        assertThat(exception.getMessage()).isEqualTo(INVALID_MAIL.message);
    }

    @Test
    public void validateWithMissingMandatorySubjectTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("from@test.com");
        input.setTo("to@test.com");
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "subject null");
    }

    @Test
    public void validateWithMissingMandatoryTextTest() {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("from@test.com");
        input.setTo("to@test.com");
        input.setSubject("[Subject]");
        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);
        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "text null");
    }

    @Test
    public void validateInput() throws ContractValidationException {
        final MailSenderInput input = new MailSenderInput();
        input.setFrom("from@test.com");
        input.setTo("to@test.com");
        input.setSubject("[Subject]");
        input.setText("test");

        input.validate();
        assertThat(input.toString()).isNotNull();
    }
}
